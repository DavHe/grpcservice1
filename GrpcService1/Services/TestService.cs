using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrpcService1
{
    public class TestService : Test.TestBase
    {
        private readonly ILogger<TestService> _logger;
        public TestService(ILogger<TestService> logger)
        {
            _logger = logger;
        }

        public override Task<TestReply> TestMsg(TestRequest request, ServerCallContext context)
        {
            return Task.FromResult(new TestReply
            {
                Message = "Hello " + request.Name
            });
        }
    }
}
